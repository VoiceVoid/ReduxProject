import {ITodo} from './todo';
import { ADD_TODO, TOGGLE_TODO, REMOVE_TODO, REMOVE_ALL_TODOS} from './actions';

//we create the interface that we will use througout this project
export interface IAppState {
todos: ITodo[];
lastUpdate: Date;
}
//this is the first state, empty array and no date we use the interface
export const INITIAL_STATE: IAppState = {
    todos: [],
    lastUpdate: null
}

export function rootReducer(state, action) {
    switch(action.type) {
        case ADD_TODO:
        //initially is 0, so only here we add the id one more and after that we assign it to new object
        action.todo.id = state.todos.length + 1;
        //we return a new object, with new state(object(data)), 
        return Object.assign({}, state, {
            todos: state.todos.concat(Object.assign({}, action.todo)),
            lastUpdate: new Date()
        });

        case TOGGLE_TODO:
        //we grab the id that we clicked in component
        const todo = state.todos.find(t => t.id === action.id);
        const index = state.todos.indexOf(todo);
        return Object.assign({}, state, {
            //we only take Todo with selected index
            todos: [
                //we take out all the objects before the one we need
                ...state.todos.slice(0, index),
                Object.assign({}, todo, {
                    isCompleted: !todo.isCompleted
                }),
                //and we take out all the object after we need
                ...state.todos.slice(index + 1)
            ],
            lastUpdate: new Date()
        });

        case REMOVE_TODO:
        return Object.assign({}, state, {
            todos: state.todos.filter(t => t.id !== action.id),
            lastUpdate: new Date()
        });

        case REMOVE_ALL_TODOS:
        return Object.assign({}, state, {
            todos: [],
            lastUpdate: new Date()
        });
    }
    return state;
}